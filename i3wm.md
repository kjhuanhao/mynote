[TOC]

# 介绍

这是一期关于manjaro的安装到开发环境配置`i3wm`的视频.我即将在manjaro_gnome里面演示

## 主要的软件或者工具

- i3wm
- tmux
- nvim
- polybar
- compton
- ranger
- rofi
- fish
- alacritty

## 展示我的桌面

![desktop.png](https://cdn.jsdelivr.net/gh/kjhuanhao/dotfiles/.img/desktop.png)

![i3wm](https://cdn.jsdelivr.net/gh/kjhuanhao/dotfiles/.img/i3.png)

# 你需要做的

备份好你自己的文件,开始和我一起安装和配置一个美观实用的开发环境

# 安装篇

**Windwos下制作一个启动盘**

- 事前准备
  需要一个至少4G以上的U盘
  1.下载[rufus](https://rufus.ie/)
  2.使用DD写入即可
  3.重启电脑启动U盘
  <br>

**Linux下制作启动盘**

```shell
sudo fdisk -l   #查看U盘路径
sudo umount /dev/sdb*   #取消挂载U盘
sudo mkfs.fat /dev/sdb -I  #格式化U盘
```

然后用dd命令

```shell
sudo dd if=镜像路径 of=/dev/sdb(启动盘)  #写入镜像
```

## 分区

> 1. 挂载点/；主分区；安装系统和软件；分区格式为ext4； 
> 2. 挂载点/home；逻辑分区；相当于“我的文档”； 分区格式ext4 
> 3. swap；逻辑分区；充当虚拟内存；大小可以等于内存大小；分区格式为swap 
> 4. /boot ；引导分区；逻辑分区；；分区格式为ext4；

建议第一次装linux的新手去百度了解一下

## 安装后的第一件事情

- 几乎所有linux你安装完成之后,我们都要进行更换镜像源的操作

```shell
sudo pacman-mirrors -i -c China -m rank  # 选ustc科大镜像源
```

<br>

- 然后进行更新

```shell
sudo pacman -Syy
sudo pacman -Syyu
```

<br>

- 在/etc/pacman.conf加入

```shell
[archlinuxcn]
SigLevel = Optional TrustedOnly
Server = https://mirrors.ustc.edu.cn/archlinuxcn/$arch
```

<br>

- 导入秘钥

```
sudo pacman -Syy
sudo pacman -S archlinuxcn-keyring 
```

<br>

## pacman相关的命令

pacman用法和介绍: https://wiki.archlinux.org/index.php/Pacman

pacman视频介绍(来自TheCW): https://www.bilibili.com/video/av55190132

## 其他问题和建议

- 如果你系统用的是中文,你还需要安装一个字体,避免中文乱码

```shell
sudo pacman -S wqy-microhei 
```



> archwiki: https://wiki.archlinux.org/
>
> archwiki(中文): https://wiki.archlinux.org/index.php/Arch_Linux_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)
>
> 提问的智慧:http://doc.zengrong.net/smart-questions/cn.html
>
> 遇到找不到的软件包看AUR,这是一个庞大的软件库
>
> aur:https://aur.archlinux.org/



# 配置篇

## 安装fish

- 安装fish

https://fishshell.com/

```shell
sudo pacman -S fish
```

- 查看fish位置
- 更换fish  chsh -s
- 安装oh-my-fish插件

```shell
curl -L https://get.oh-my.fish | fish


```

- 配置fish主题

## 安装i3

i3是一个窗口管理器,可以高效管理你的桌面,管理你的窗口,窗口直接可以快速地切换,再配合一些小工具,除了浏览网页之类的,几乎是用不到鼠标,可以大大增加了你的工作效率

那么如何去安装和使用i3呢?在这之前我来带你安装一下linux

想要高效就先拥有一个好用的系统,例如archlinux,或者arch的发行版,衍生版

arch下的aur我觉得几乎是万能的,我从来没有遇过什么软件包不存在这种问题

我这里就演示如何安装manjaro

```shell
sudo pacman -S i3


```

- 重启
- 选择i3

## 安装rofi

rofi

```shell
sudo pacman -S rofi


```

- clone 我的仓库dotfiles

https://github.com/kjhuanhao/dotfiles.git

移动配置文件到 ~目录下

## 安装依赖

建议手动安装,因为脚本是一键式,容易出现部分没有安装问题,不容易差错

## 安装tmux和alacrity

tmux是一个终端复用器,类似GNU Screen

Alacritty是一个终端模拟器

```shell
sudo pacman -S tmux alacritty


```

移动tmux和alacrity的配置文件

tmux移动到~ 的.tmux.conf下

移动主题文件到~下

## 安装polybar

```shell
sudo pacman -S polybar


```

移动polyabr配置文件

如果是虚拟机你无法使用我的的polybar配置,只能使用默认的

可以去看看默认的配置在哪,记得生成laught

## 安装nvim

```shell
sudo pacman -S neovim


```

## 移动i3配置文件

- 复制
- 到.config/i3下修改分辨率加上virual1
- 重载i3

## 安装compton

```shell
sudo pacman -S compton


```

建议去github看官方的安装方式,如果报错那就是缺少依赖

## 关于切换键位布局

在我的配置文件里面有一份colemak的键位布局和Qwert键盘布局

你可以运行脚本直接替换

# 其他

推荐UP主:TheCW

本视频提到的一些东西,大多都可以在TheCW的视频找到相关的介绍

- 新手建议

解决问题大法:

遇到软件使用问题看[archwiki]( https://wiki.archlinux.org/)

archwiki没有找到,没有关系

再看github的,对应仓库wiki

没有找到,没有关系

看看issues,有没有相同问题

遇到ERROR,不要着急

先查[百度](https://www.baidu.com/),或者[bing](https://cn.bing.com/)

就算[谷歌](https://www.google.com/),也没有关系

仍未解决,切莫放弃

发个problem,who can help me

知乎,贴吧,群里也发

csdn也能考虑

提问之前必须think

[提问的智慧](http://doc.zengrong.net/smart-questions/cn.html)要牢记

许多途径切勿放弃!